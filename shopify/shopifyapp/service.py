from const import SHOPIFY_CREDENTIALS

def getShopifyURl():
    API_KEY = SHOPIFY_CREDENTIALS['API_KEY']
    API_PASSWORD = SHOPIFY_CREDENTIALS['API_PASSWORD']
    BUSINESS_NAME = SHOPIFY_CREDENTIALS['BUSINESS_NAME']
    return "https://" + API_KEY + ":" + API_PASSWORD + "@" + BUSINESS_NAME