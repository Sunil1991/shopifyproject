from django.shortcuts import render
import requests
from const import SHOPIFY_CREDENTIALS
import json
from django.http import HttpResponse
from productjsonconfig import product_default_format_and_image
from service import getShopifyURl
from django.views.decorators.csrf import csrf_exempt


# Create your views here.


def getAllProducts(request):
    try:
        if request.method == 'GET':
            template = 'home.html'
            # template = '/tempaltes/shopifyapp/home.html'
            # template = '/shopifyapp/home.html'
            URL=getShopifyURl()+".myshopify.com/admin/products.json"
            response = requests.get(URL)
            response_data = {}
            if response.status_code == 200:
                status = 'Success'
                products = dict(response.json())
                return render(request, template, {'status':'sucess','products':products})

            else:
                status = 'error'
                products = dict(response.json())
                return render(request, template,{'status':'sucess','products':products})
            #return HttpResponse(, content_type="application/json")
    except Exception as e:
        print(e)

@csrf_exempt
def addProduct(request):
    try:
        if request.method == 'POST':
            title = request.POST.get('title')
            body = request.POST.get('body')
            image = request.POST.get('image')
            if image:
                image = image[image.rfind('base64,')+7:]
            vendor = request.POST.get('vendor')
            type = request.POST.get('type')
            product_default_format_and_image['product']['title'] = title
            product_default_format_and_image['product']['body_html'] = body
            product_default_format_and_image['product']['vendor'] = vendor
            product_default_format_and_image['product']['product_type'] = type
            product_default_format_and_image['product']['images'][0]['attatchment'] = image
            URL = getgetShopifyURl()+".myshopify.com/admin/products.json"
            #request = requests.get(URL)
            payload = json.dumps(product_default_format_and_image)
            payload = json.loads(payload)
            request = requests.post(URL, json=payload)
            if request.status_code == 201:
                return HttpResponse(request, content_type="application/json")
    except Exception as e:
        print(e)